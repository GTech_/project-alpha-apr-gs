from django.urls import reverse_lazy
from tasks.models import Task
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template = "tasks/task_list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/new.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def get_success_url(self) -> str:
        return reverse_lazy("show_project", args=[self.object.id])


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template = "tasks/task_list.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
